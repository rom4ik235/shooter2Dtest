using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using UnityEngine.UIElements;

namespace Inventory
{

    // Этот класс определяет пользовательский редактор для вашего объекта
    [CustomEditor(typeof(Storage))]
    public class InventoryStorageEditor : Editor
    {
        private bool showStorage = false;
        static private readonly string showStorageKey = "Inventory.Storage.Show";
        static private readonly string PrefKey = "Inventory.Storage.Items.Keys";
        private List<Item> _Items = new();
        private List<Item> Items
        {
            get
            {
                if (Application.isPlaying)
                {
                    Storage Storage = (Storage)target;
                    return Storage.Items;
                }
                else
                    return _Items;
            }
        }

        void ChangeItemCount(Item selectItem, int count)
        {
            if (Application.isPlaying)
            {
                Storage Storage = (Storage)target;
                Storage.ChangeItemCount(selectItem, count);
            }
            else
            {
                selectItem.Remove(count);
                if (selectItem.Count <= 0)
                    Items.Remove(selectItem);
            }
        }

        void AddItem(Item item)
        {
            if (Application.isPlaying)
            {
                Storage Storage = (Storage)target;
                Storage.AddItem(item);
            }
            else
            {
                Items.Add(item);
            }
        }


        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            showStorage = EditorGUILayout.BeginFoldoutHeaderGroup(showStorage, "Storage");
            if (showStorage)
            {
                EditorGUI.indentLevel++;
                for (int i = 0; i < Items.Count; i++)
                {
                    var item = Items[i];
                    GUILayout.Label(
                        $"<color=#FFFFFF>Name: <b>{item.Data.Name}</b></color>",
                        new GUIStyle() { richText = true }
                    );
                    EditorGUILayout.BeginVertical();
                    EditorGUI.indentLevel++;
                    GUILayout.Label($"State: {item.State}");
                    GUILayout.Label($"Count: {item.Count}");
                    string[] removeButtons = new[] { "-10", "-5", "-1", "+1", "+5", "+10" };
                    int selected = GUILayout.Toolbar(-1, removeButtons);
                    if (selected >= 0)
                        ChangeItemCount(item, -int.Parse(removeButtons[selected]));
                    EditorGUI.indentLevel--;
                    EditorGUILayout.EndVertical();
                    GUILayout.Space(20);
                }
                EditorGUI.indentLevel--;
            }
            EditorGUILayout.EndFoldoutHeaderGroup();

            var NewData = (Data.ItemObject)EditorGUILayout.ObjectField("Add Item", null, typeof(Data.ItemObject), false);
            if (NewData != null)
            {
                if (NewData is Data.WeaponItemObject weapon)
                    AddItem(new WeaponItem(weapon));
                else if (NewData is Data.ClothItemObject cloth)
                    AddItem(new ClothItem(cloth));
            }
        }

        List<T> LoadScriptableObjectsByClassName<T>() where T : ScriptableObject
        {
            var scriptableObjectsList = new List<T>();
            string[] guids = AssetDatabase.FindAssets($"t:{typeof(T).Name}");

            foreach (string guid in guids)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(guid);
                T obj = AssetDatabase.LoadAssetAtPath(assetPath, typeof(T)) as T;
                if (obj != null && obj.GetType().Name == typeof(T).Name)
                    scriptableObjectsList.Add(obj);
            }
            return scriptableObjectsList;
        }

        private void OnEnable() { LoadData(); }

        private void OnDisable() { SaveData(); }

        private void LoadData()
        {
            showStorage = EditorPrefs.GetBool(showStorageKey, false);
            _Items = LoadStorageItems();
        }

        private void SaveData()
        {
            string keys = "";
            for (int i = 0; i < _Items.Count; i++)
            {
                var item = _Items[i];
                string jsonData = JsonUtility.ToJson(item);
                string itemKey = item.GetType().Name + i;
                EditorPrefs.SetString(itemKey, jsonData);
                keys += $"{itemKey}|";
            }
            EditorPrefs.SetString(PrefKey, keys);
            EditorPrefs.SetBool(showStorageKey, showStorage);
        }

        static public List<Item> LoadStorageItems()
        {
            string KeysString = EditorPrefs.GetString(PrefKey, "");
            var listKeys = KeysString.Split('|');
            List<Item> Items = new();
            foreach (var key in listKeys)
            {
                string jsonData = EditorPrefs.GetString(key);
                if (!string.IsNullOrEmpty(jsonData))
                {
                    if (key.Contains(typeof(WeaponItem).Name))
                    {
                        var data = JsonUtility.FromJson<WeaponItem>(jsonData);
                        if (data.Data == null) continue;
                        Items.Add(data);
                    }
                    else if (key.Contains(typeof(ClothItem).Name))
                    {
                        var data = JsonUtility.FromJson<ClothItem>(jsonData);
                        if (data.Data == null) continue;
                        Items.Add(data);
                    }
                }
            }
            return Items;
        }
    }

}