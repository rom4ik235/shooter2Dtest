using System;


namespace Inventory
{
    public class WeaponItem : Item
    {
        public WeaponItem(Data.WeaponItemObject data, int count = 1, ItemState state = ItemState.Default) : base(data, count, state) { }


    }
    public class ClothItem : Item
    {
        public ClothItem(Data.ClothItemObject data, int count = 1, ItemState state = ItemState.Default) : base(data, count, state) { }


    }
}

