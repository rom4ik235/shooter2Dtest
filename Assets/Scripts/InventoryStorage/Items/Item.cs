using System;
using UnityEngine;


namespace Inventory
{
    [Serializable]
    abstract public class Item : IEquatable<Item>
    {
        [field: SerializeField] public Data.ItemObject Data { get; private set; }
        [field: SerializeField] public int Count { get; private set; }
        [field: SerializeField] public ItemState State { get; private set; }

        public void Add(int count = 1) => Count += count;
        public void Remove(int count = 1) => Count -= count;
        public void SetState(ItemState state) => State = state;

        public bool Equals(Item other)
        {
            return Data.Equals(other.Data) &&
            State.Equals(other.State) &&
            Count.Equals(other.Count);
        }

        public Item(Data.ItemObject data, int count = 1, ItemState state = ItemState.Default)
        {
            Data = data;
            Count = count;
            State = state;
        }
    }
}

