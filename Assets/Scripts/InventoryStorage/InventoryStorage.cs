using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace Inventory
{

    public class Storage : MonoBehaviour
    {
        public List<Item> Items { get; private set; } = new();
        public readonly UnityEvent<Item> OnInventoryItemRemove = new();
        public readonly UnityEvent<Item> OnInventoryItemAdd = new();
        public readonly UnityEvent<Item> OnInventoryItemUpdate = new();
        public readonly UnityEvent OnInventoryUpdate = new();

        private Player Player;

        private void Start()
        {
            Items = InventoryStorageEditor.LoadStorageItems();
            Player = GetComponentInParent<Player>();
        }

        public void AddItem(Item newItem)
        {
            foreach (var item in Items)
            {
                if (item.Data == newItem.Data)
                {
                    item.Add();
                    OnInventoryItemUpdate.Invoke(item);
                    return;
                }
            }
            Items.Add(newItem);
            OnInventoryItemAdd.Invoke(newItem);
        }

        public void ChangeItemCount(Item selectItem, int count = 1)
        {
            foreach (var item in Items)
            {
                if (item.Data == selectItem.Data)
                {
                    item.Remove(count: count);
                    if (item.Count <= 0)
                    {
                        Items.Remove(selectItem);
                        OnInventoryItemRemove.Invoke(selectItem);
                    }
                    else
                        OnInventoryItemUpdate.Invoke(item);
                    break;
                }
            }

        }

        public void TapItem(Item item)
        {
            if (item.Data is Data.WeaponItemObject weaponData)
            {
                switch (item.State)
                {
                    case ItemState.Default:
                        Player.EquipWeapon(weaponData.Prefab);
                        foreach (var element in Items)
                            if (element.State == ItemState.Equip)
                            {
                                element.SetState(ItemState.Default);
                                OnInventoryItemUpdate.Invoke(element);
                            }
                        item.SetState(ItemState.Equip);
                        OnInventoryItemUpdate.Invoke(item);
                        break;
                    case ItemState.Selected:
                    case ItemState.Equip:
                        Player.UnequipWeapon();
                        item.SetState(ItemState.Default);
                        OnInventoryItemUpdate.Invoke(item);
                        break;
                }
            }
            else if (item.Data is Data.ClothItemObject clothData) { }
            else if (item.Data is Data.DefaultItemObject defaultData) { }
            else if (item.Data is Data.AmmoItemObject ammoData) { }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.transform.TryGetComponent<Drop>(out var drop))
            {
                if (drop.itemObject is Data.WeaponItemObject weapon)
                    AddItem(new WeaponItem(weapon));
                Destroy(drop.gameObject);
            }
        }

    }


    public enum ItemState
    {
        Default,
        Selected,
        Equip,
    }
}

