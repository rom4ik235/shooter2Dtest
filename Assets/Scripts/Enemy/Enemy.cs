using System;
using FSM.Scripts;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _hp;
    [SerializeField] private SpriteRenderer _hpBar;
    private float _maxHp;

    private Rigidbody2D _rigidbody;
    private Collider2D _collider;
    private Animator _animator;
    private Fsm<Enemy> Fsm;
    public Vector2? target = null;

    private StateEnum AnimationState { get; set; } = StateEnum.Idle;
    public bool Flip { get; private set; } = false;

    public StateEnum GetAnimationState() => AnimationState;

    void Start()
    {
        _maxHp = _hp;
        _animator = GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
        Fsm = new(this);

        Fsm.AddState(new EnemyIdleState(Fsm, _animator));
        Fsm.AddState(new EnemyWalkState(Fsm, _animator, _rigidbody));
        Fsm.AddState(new EnemyAttackState(Fsm, _animator));
        Fsm.AddState(new EnemyDamageState(Fsm, _animator));
        Fsm.AddState(new EnemyDeathState(Fsm, _animator));

        Fsm.SetState<EnemyIdleState>();
    }

    void Update() => Fsm.CurrentState.Update();

    void FixedUpdate() => Fsm.CurrentState.FixedUpdate();

    public void SetFlip(bool value)
    {
        Flip = value;
        transform.Rotate(0, 180, 0);
    }

    public void Damage(int Damage)
    {
        if (Fsm.CurrentState is EnemyDeathState) return;
        _hp -= Damage;
        var localScale = _hpBar.transform.localScale;
        localScale.x = _hp / _maxHp;
        _hpBar.transform.localScale = localScale;
        if (_hp < 0) _hp = 0;
        if (_hp == 0) Death();
        else Fsm.SetState<EnemyDamageState>();
    }

    public void Death()
    {
        Fsm.SetState<EnemyDeathState>();
        Destroy(_collider);
        Destroy(_rigidbody);
        Destroy(gameObject, 2.0f);
    }

    public enum StateEnum
    {
        Attack,
        Damage,
        Death,
        Walk,
        Idle,
    }
}
