using UnityEngine;

namespace FSM.Scripts
{
    public class EnemyWalkState : FsmState<Enemy>
    {
        readonly Animator animator;
        readonly Rigidbody2D rigidbody;
        public EnemyWalkState(Fsm<Enemy> fsm, Animator animator, Rigidbody2D rigidbody) : base(fsm)
        {
            this.animator = animator;
            this.rigidbody = rigidbody;
        }

        public override void Enter()
        {
            animator.SetBool("Walk", true);
        }

        public override void Update()
        {

            if (Fsm.behaviour.target != null)
            {
                var target = (Vector2)Fsm.behaviour.target;
                var heading = target - (Vector2)Fsm.behaviour.transform.position;
                var distance = heading.magnitude;
                var direction = heading / distance;
                Move(direction, 2);
                if (heading.x < 0 && !Fsm.behaviour.Flip) Fsm.behaviour.SetFlip(!Fsm.behaviour.Flip);
                else if (heading.x > 0 && Fsm.behaviour.Flip) Fsm.behaviour.SetFlip(!Fsm.behaviour.Flip);
                if (distance <= 0.1)
                {
                    Fsm.behaviour.target = null;
                    Fsm.SetState<EnemyIdleState>();
                }
            }
            else
            {
                Fsm.SetState<EnemyIdleState>();
            }
        }

        protected void Move(Vector2 direction, float speed)
        {
            rigidbody.velocity = direction * speed;
        }
    }
}