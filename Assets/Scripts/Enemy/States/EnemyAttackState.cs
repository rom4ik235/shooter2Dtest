using UnityEngine;

namespace FSM.Scripts
{
    public class EnemyAttackState : FsmState<Enemy>
    {
        readonly Animator animator;
        public EnemyAttackState(Fsm<Enemy> fsm, Animator animator) : base(fsm)
        {
            this.animator = animator;
        }
        public override void Enter()
        {
            animator.SetTrigger("Attack");
        }

        public override void Update()
        {
            switch (Fsm.behaviour.GetAnimationState())
            {
                case Enemy.StateEnum.Walk:
                    Fsm.SetState<EnemyWalkState>();
                    break;
                case Enemy.StateEnum.Idle:
                    Fsm.SetState<EnemyIdleState>();
                    break;
            }
        }
    }
}