
using UnityEngine;

namespace FSM.Scripts
{
    public class EnemyIdleState : FsmState<Enemy>
    {
        readonly Animator animator;
        public EnemyIdleState(Fsm<Enemy> fsm, Animator animator) : base(fsm)
        {
            this.animator = animator;
        }

        public override void Enter()
        {
            animator.SetBool("Walk", false);
            if (Fsm.behaviour.target == null)
            {
                Vector2 vector2 = (Vector2)Fsm.behaviour.transform.position + (Random.insideUnitCircle * 5);
                Fsm.behaviour.target = vector2;
            }
            Fsm.SetState<EnemyWalkState>();
        }

    }
}