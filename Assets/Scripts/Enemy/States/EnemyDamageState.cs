using UnityEngine;

namespace FSM.Scripts
{
    public class EnemyDamageState : FsmState<Enemy>
    {
        readonly Animator animator;
        float time = 0;
        public EnemyDamageState(Fsm<Enemy> fsm, Animator animator) : base(fsm)
        {
            this.animator = animator;
        }

        public override void Enter()
        {
            time = 0.5f;
            animator.SetTrigger("Damage");
        }

        public override void Update()
        {
            if (time > 0)
            {
                time -= Time.deltaTime;
                return;
            }
            switch (Fsm.behaviour.GetAnimationState())
            {
                case Enemy.StateEnum.Walk:
                    Fsm.SetState<EnemyWalkState>();
                    break;
                case Enemy.StateEnum.Idle:
                    Fsm.SetState<EnemyIdleState>();
                    break;
            }
        }
    }
}