using Unity.VisualScripting;
using UnityEngine;

namespace FSM.Scripts
{
    public class EnemyDeathState : FsmState<Enemy>
    {
        readonly Animator animator;
        public EnemyDeathState(Fsm<Enemy> fsm, Animator animator) : base(fsm)
        {
            this.animator = animator;
        }

        public override void Enter()
        {
            animator.SetTrigger("Death");
        }

    }
}