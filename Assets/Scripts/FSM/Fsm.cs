
using System;
using System.Collections.Generic;
using FSM.Scripts;
using UnityEngine;

/// <summary>
/// Машина финальных состояний.
/// Для упрощения работы с логикой путем разделения ее на разные состояния 
/// </summary>
/// <typeparam name="B"></typeparam>
public class Fsm<B> where B : MonoBehaviour
{
    /// <summary>
    /// Обьект к которому привязан Fsm.
    /// Для вызова функций общих для всех состояний.
    /// </summary>
    readonly public B behaviour;
    public Fsm(B behaviour) { this.behaviour = behaviour; }

    public FsmState<B> CurrentState { get; private set; }
    private readonly Dictionary<Type, FsmState<B>> _states = new();
    private readonly Dictionary<Type, FsmSignal> _signals = new();

    /// <summary> Иницализация сотояний</summary>
    /// <param name="state">Класс описывающий состояние</param>
    public void AddState(FsmState<B> state) => _states.Add(state.GetType(), state);


    /// <summary> Установить состояние </summary>
    /// <typeparam name="T">Тип состояния</typeparam>
    public void SetState<T>() where T : FsmState<B>
    {
        var type = typeof(T);

        if (CurrentState != null && CurrentState.GetType() == type) return;

        if (_states.TryGetValue(type, out var newState))
        {
            CurrentState?.Exit();
            CurrentState = newState;
            CurrentState.Enter();
        }
    }

    /// <summary> Получить последний сигнал или null если его еще не было </summary>
    public bool TryGetSignal<T>(out T value) where T : FsmSignal
    {
        if (_signals.TryGetValue(typeof(T), out var signal))
        {
            value = signal as T;
            return true;
        }
        value = null;
        return false;
    }

    /// <summary> Получить последний сигнал или null если его еще не было </summary>
    public T GetSignal<T>() where T : FsmSignal
    {
        if (_signals.TryGetValue(typeof(T), out var signal))
            return signal as T;
        return null;
    }

    /// <summary>
    /// Вызвать сигнал у текущего состояния <paramref name="CurrentState"/>
    /// </summary>
    public void Signal<T>(T signalData) where T : FsmSignal
    {
        _signals[signalData.GetType()] = signalData;
        if (CurrentState is ISignalHandler<T> handler)
            handler.Signal(signalData);
    }
}
