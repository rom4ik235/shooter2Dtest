using UnityEngine;

namespace FSM.Scripts
{
    public abstract class FsmState<B> where B : MonoBehaviour
    {
        protected readonly Fsm<B> Fsm;

        public FsmState(Fsm<B> fsm) { Fsm = fsm; }

        public virtual void Enter() { }
        public virtual void Exit() { }
        public virtual void Update() { }
        public virtual void FixedUpdate() { }
    }

    public interface ISignalHandler<T>
    {
        public void Signal(T signalData);
    }
}
