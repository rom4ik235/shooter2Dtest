using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryPanel : MonoBehaviour, ICallbackButtonHandler
{
    [SerializeField] private InventoryCell Prefab;
    [SerializeField] private Vector2 Margin;
    private readonly List<InventoryCell> _cells = new();
    private RectTransform _rectTransform;
    private GridLayoutGroup _gridLayoutGroup;
    private Inventory.Storage Inventory;


    void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        _gridLayoutGroup = GetComponent<GridLayoutGroup>();
        Inventory = FindObjectOfType<Inventory.Storage>();
        OnInventoryUpdate();
        Inventory.OnInventoryUpdate.AddListener(OnInventoryUpdate);
        Inventory.OnInventoryItemUpdate.AddListener(OnInventoryItemUpdate);
        Inventory.OnInventoryItemRemove.AddListener(OnInventoryItemRemove);
        Inventory.OnInventoryItemAdd.AddListener(OnInventoryItemAdd);
    }

    void OnInventoryItemRemove(Inventory.Item item)
    {
        foreach (var cell in _cells)
        {
            if (cell.Item.Equals(item))
            {
                _cells.Remove(cell);
                Destroy(cell.gameObject);
                Resize();
                return;
            }
        }
    }

    void OnInventoryItemAdd(Inventory.Item item)
    {
        var cell = Instantiate(Prefab, transform);
        _cells.Add(cell);
        cell.Render(item);
        Resize();
    }

    void OnInventoryItemUpdate(Inventory.Item item)
    {
        foreach (var cell in _cells)
        {
            if (cell.Item.Equals(item))
            {
                cell.Render(item);
                return;
            }
        }
    }

    void OnInventoryUpdate()
    {
        _cells.Clear();
        int childCount = transform.childCount;
        for (int i = childCount - 1; i >= 0; i--)
            Destroy(transform.GetChild(i).gameObject);
        for (int i = 0; i < Inventory.Items.Count; i++)
        {
            _cells.Add(Instantiate(Prefab, transform));
            _cells.Last().Render(Inventory.Items[i]);
        }
        Resize();
    }

    private void OnValidate()
    {
        _rectTransform = GetComponent<RectTransform>();
        _gridLayoutGroup = GetComponent<GridLayoutGroup>();
        Resize();
    }

    void Resize()
    {
        int count = transform.childCount;
        if (count <= 0)
        {
            _rectTransform.sizeDelta = Vector2.zero;
            return;
        }
        var width = _gridLayoutGroup.cellSize.x + _gridLayoutGroup.spacing.x;
        var padding = new Vector2(_gridLayoutGroup.padding.left + _gridLayoutGroup.padding.right, _gridLayoutGroup.padding.top + _gridLayoutGroup.padding.bottom);
        var height = _gridLayoutGroup.cellSize.y + _gridLayoutGroup.spacing.y;
        _rectTransform.sizeDelta = new Vector2((width * count) - _gridLayoutGroup.spacing.x, height) + padding;
        _rectTransform.anchoredPosition = new Vector2(
            -(_rectTransform.sizeDelta.x / 2) + Margin.x,
            (_rectTransform.sizeDelta.y / 2) + Margin.y
        );
        // _cells.Sort(SortByEnum);
        // for (int i = 0; i < _cells.Count; i++)
        //     _cells[i].transform.SetSiblingIndex(i);
    }

    private int SortByEnum(InventoryCell item1, InventoryCell item2)
    {
        return item1.Item.Data.Type().CompareTo(item2.Item.Data.Type());
    }

    public void OnButtonClick(PointerEventData eventData, CallbackParentButton button)
    {
        var cell = button.GetComponent<InventoryCell>();
        if (cell.Item != null)
            Inventory.TapItem(cell.Item);
    }
}
