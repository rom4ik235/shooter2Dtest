using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryCell : CallbackParentButton
{

    [SerializeField] private Text CountText;
    [Space]
    [SerializeField] Image image;
    [SerializeField] Image categoryIcon;
    [field: SerializeField] public Image BackgroundImage { get; private set; }
    private Color ColorDefault;
    [field: SerializeField] Color ColorEquip;
    [field: SerializeField] Color ColorSelected;
    [NonSerialized] public Inventory.Item Item = null;

    public void Render(Inventory.Item item)
    {
        BackgroundImage = GetComponent<Image>();
        if (ColorDefault == null)
            ColorDefault = BackgroundImage.color;

        if (Item != item)
        {
            Item = item;
            if (Item == null) return;
            transform.name = $"{Item.Data.Name}Cell";

            if (Item.Data is Data.WeaponItemObject weaponData)
            {
                image.sprite = weaponData.Image;
                categoryIcon.sprite = weaponData.Icon;
            }
            else if (Item.Data is Data.ClothItemObject clothData)
            {
                image.sprite = clothData.Image;
                categoryIcon.sprite = clothData.Icon;
            }
            else if (Item.Data is Data.DefaultItemObject defaultData)
            {
                // image.sprite = defaultData.Image;
                categoryIcon.sprite = defaultData.Icon;
            }
            else if (Item.Data is Data.AmmoItemObject ammoData)
            {
                // image.sprite = ammoData.Image;
                categoryIcon.sprite = ammoData.Icon;
            }
        }
        CountText.text = Item.Count.ToString();
        BackgroundImage.color = GetBackgroundColor(Item.State);

    }

    Color GetBackgroundColor(Inventory.ItemState state)
    {
        return state switch
        {
            Inventory.ItemState.Default => ColorDefault,
            Inventory.ItemState.Equip => ColorEquip,
            Inventory.ItemState.Selected => ColorSelected,
            _ => ColorDefault,
        };
    }
}
