using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

public class CallbackParentButton : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    ICallbackButtonHandler parent;

    void Start()
    {
        parent = GetComponentInParent<ICallbackButtonHandler>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (parent != null)
            parent.OnButtonClick(eventData, this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (parent != null)
            parent.OnButtonEnter(eventData, this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (parent != null)
            parent.OnButtonExit(eventData, this);
    }
}

interface ICallbackButtonHandler
{
    public void OnButtonClick(PointerEventData eventData, CallbackParentButton button) { }
    public void OnButtonEnter(PointerEventData eventData, CallbackParentButton button) { }
    public void OnButtonExit(PointerEventData eventData, CallbackParentButton button) { }
}
