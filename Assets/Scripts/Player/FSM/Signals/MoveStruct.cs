using UnityEngine;

namespace FSM.Scripts.Players.Signals
{
    public class MoveSignal : FsmSignal
    {
        public readonly Vector2 vector;
        public readonly float speed;

        public MoveSignal(Vector2 vector, float speed)
        {
            this.vector = vector;
            this.speed = speed;
        }
    }
}