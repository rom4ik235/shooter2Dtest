
namespace FSM.Scripts.Players
{

    public class PlayerIdleState : FsmState<Player>, ISignalHandler<Signals.MoveSignal>
    {
        public PlayerIdleState(Fsm<Player> fsm) : base(fsm) { }

        public void Signal(Signals.MoveSignal move)
        {
            if (move.vector.sqrMagnitude != 0.0f)
            {
                Fsm.SetState<PlayerWalkState>();
                Fsm.Signal(move);
            }
        }
    }
}