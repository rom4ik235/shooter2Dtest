using UnityEngine;

namespace FSM.Scripts.Players
{

    public class PlayerWalkState : FsmState<Player>, ISignalHandler<Signals.MoveSignal>
    {

        readonly Rigidbody2D rigidbody;
        public PlayerWalkState(Fsm<Player> fsm, Rigidbody2D rigidbody, Animator animator) : base(fsm)
        {
            this.animator = animator;
            this.rigidbody = rigidbody;
        }

        readonly Animator animator;

        public override void Enter()
        {
            animator.SetBool("Walk", true);
        }

        public override void Exit()
        {
            animator.SetBool("Walk", false);
        }

        public void Signal(Signals.MoveSignal move)
        {
            if (move.vector.sqrMagnitude == 0.0f)
                Fsm.SetState<PlayerIdleState>();
            Move(move.vector, move.speed);
        }

        protected void Move(Vector2 direction, float speed)
        {
            rigidbody.velocity = direction * speed;
        }
    }
}