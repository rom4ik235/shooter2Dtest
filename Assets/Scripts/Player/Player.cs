using System.Collections.Generic;
using System.Linq;
using FSM.Scripts.Players;
using FSM.Scripts.Players.Signals;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.U2D.IK;


[RequireComponent(typeof(Animator))]
public class Player : MonoBehaviour
{
    [SerializeField] private LimbSolver2D _ArmL;
    [SerializeField] private LimbSolver2D _ArmR;
    [SerializeField] private Transform _WeaponPoint;
    [SerializeField] private Animator _Animator;
    [SerializeField] private float _Speed;
    [SerializeField] private Inventory.Storage _Inventory;
    private Weapon _Weapon = null;
    private Rigidbody2D _rigidbody2d;
    private Fsm<Player> _Fsm;
    private bool _flip = false;

    private readonly List<Collider2D> _Enemies = new();

    private void Start()
    {
        _rigidbody2d = GetComponent<Rigidbody2D>();
        _Fsm = new(this);
        _Fsm.AddState(new PlayerIdleState(_Fsm));
        _Fsm.AddState(new PlayerWalkState(_Fsm, _rigidbody2d, _Animator));
        _Fsm.SetState<PlayerIdleState>();
        if (_Weapon != null) EquipWeapon(_Weapon);
        else UnequipWeapon();
    }

    public void UnequipWeapon()
    {
        if (_Weapon != null && _Weapon.gameObject.activeInHierarchy)
            Destroy(_Weapon.gameObject);
        _Weapon = null;
        _ArmL.weight = 0;
        _ArmR.weight = 0;
    }

    public void EquipWeapon(Weapon Weapon)
    {
        Vector2 oldRight = _Animator.transform.right;
        if (_Weapon != null && Weapon != _Weapon)
        {
            oldRight = _Weapon.transform.right;
            if (_Weapon.gameObject.activeInHierarchy)
                Destroy(_Weapon.gameObject);
        }
        if (Weapon.gameObject.activeInHierarchy)
        {
            _Weapon = Weapon;
            _Weapon.transform.SetParent(_WeaponPoint);
        }
        else
            _Weapon = Instantiate(Weapon, _WeaponPoint);
        _Weapon.LookAt(oldRight, _flip);
        _ArmL.weight = _Weapon.LimbL != null ? 1 : 0;
        _ArmR.weight = _Weapon.LimbR != null ? 1 : 0;
        _Weapon.transform.localPosition = Vector3.zero;
        if (_Enemies.Count > 0) _Weapon.SetReady(true);
    }

    public void OnMove(InputValue value)
    {
        var vector = value.Get<Vector2>();
        _Fsm.Signal(new MoveSignal(value.Get<Vector2>(), _Speed));

        if (vector.sqrMagnitude != 0.0f)
        {
            if (vector.x < 0 && !_flip) Flip();
            else if (vector.x > 0 && _flip) Flip();
            if (_Enemies.Count == 0)
                if (_Weapon != null) _Weapon.LookAt(vector, _flip);
        }
    }

    public void OnAttack(InputValue value)
    {
        if (_Weapon != null) _Weapon.Attack(value.isPressed);
    }

    private void Update()
    {
        if (_Weapon != null)
        {
            if (_Weapon.LimbL != null)
                _ArmL.GetChain(0).target.transform.position = _Weapon.LimbL.position;
            if (_Weapon.LimbR != null)
                _ArmR.GetChain(0).target.transform.position = _Weapon.LimbR.position;
            if (_Enemies.Count > 0)
            {
                var enemy = _Enemies.First();
                var vector = enemy.bounds.center - _Weapon.transform.position;
                if (vector.x < 0 && !_flip) Flip();
                else if (vector.x > 0 && _flip) Flip();
                _Weapon.LookAt(vector, _flip);
            }
        }


        _Fsm.CurrentState.Update();
    }

    private void Flip()
    {
        _Animator.transform.Rotate(0, 180, 0);
        _flip = !_flip;
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        other.transform.TryGetComponent<Enemy>(out var enemy);
        if (enemy != null)
        {
            _Enemies.Add(other);
            if (_Weapon != null)
                _Weapon.SetReady(true);
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        other.transform.TryGetComponent<Enemy>(out var enemy);
        if (enemy != null)
        {
            _Enemies.Remove(other);
            if (_Enemies.Count == 0)
                if (_Weapon != null)
                    _Weapon.SetReady(false);
        }
    }
}
