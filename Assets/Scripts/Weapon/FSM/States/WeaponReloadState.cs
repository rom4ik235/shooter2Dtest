
using FSM.Scripts.Weapons.Signals;
using UnityEngine;

namespace FSM.Scripts.Weapons
{

    public class WeaponReloadState : FsmState<Weapon>, ISignalHandler<LookAtSignal>
    {
        readonly Animator animator;
        readonly AnimationClip reload;
        float time = 0f;
        public WeaponReloadState(Fsm<Weapon> fsm, Animator animator, AnimationClip reload) : base(fsm)
        {
            this.animator = animator;
            this.reload = reload;
        }

        public override void Enter()
        {
            animator.SetTrigger("Reload");
            time = reload.length;
        }

        public void Signal(LookAtSignal signalData)
        {
            var angle = Vector2.Angle(Vector2.right, signalData.vector);
            var transform = Fsm.behaviour.transform;
            if (signalData.flip)
                transform.eulerAngles = new Vector3(180, 0, Vector2.right.y < signalData.vector.y ? -angle : angle);
            else
                transform.eulerAngles = new Vector3(0, 0, Vector2.right.y < signalData.vector.y ? angle : -angle);
        }

        public override void Update()
        {
            if (time <= 0)
            {
                Fsm.behaviour.SetCurrentAmmo(Fsm.behaviour.Ammo);
                var signal = Fsm.GetSignal<SetReadySignal>();

                if (signal?.ready ?? false)
                    Fsm.SetState<WeaponReadyState>();
                else
                    Fsm.SetState<WeaponIdleState>();
            }
            else
                time -= Time.deltaTime;
        }
    }
}