
using FSM.Scripts.Weapons.Signals;
using UnityEngine;

namespace FSM.Scripts.Weapons
{

    public class WeaponIdleState : FsmState<Weapon>, ISignalHandler<LookAtSignal>, ISignalHandler<SetReadySignal>
    {
        readonly Animator animator;
        public WeaponIdleState(Fsm<Weapon> fsm, Animator animator) : base(fsm)
        {
            this.animator = animator;
        }


        public override void Enter()
        {
            animator.SetBool("Ready", false);
            if (Fsm.TryGetSignal<LookAtSignal>(out var signal))
                Signal(signal);
            base.Enter();
        }

        public void Signal(LookAtSignal signalData)
        {
            var transform = Fsm.behaviour.transform;
            if (signalData.flip)
                transform.right = Vector2.left;
            else
                transform.right = Vector2.right;
        }

        public void Signal(SetReadySignal signalData)
        {
            if (signalData.ready)
                Fsm.SetState<WeaponReadyState>();
        }
    }
}