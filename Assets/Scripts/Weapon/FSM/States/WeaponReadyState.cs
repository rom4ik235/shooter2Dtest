
using FSM.Scripts.Weapons.Signals;
using UnityEngine;

namespace FSM.Scripts.Weapons
{

    public class WeaponReadyState : FsmState<Weapon>, ISignalHandler<LookAtSignal>, ISignalHandler<AttackSignal>, ISignalHandler<SetReadySignal>
    {
        readonly Animator animator;
        float time = 0;
        bool attack = false;

        public WeaponReadyState(Fsm<Weapon> fsm, Animator animator) : base(fsm)
        {
            this.animator = animator;
        }

        public override void Enter()
        {
            if (Fsm.TryGetSignal<AttackSignal>(out var signal))
                attack = signal.attack;

            animator.SetBool("Ready", true);
        }

        public void Signal(LookAtSignal signalData)
        {
            var angle = Vector2.Angle(Vector2.right, signalData.vector);
            var transform = Fsm.behaviour.transform;
            if (signalData.flip)
                transform.eulerAngles = new Vector3(180, 0, Vector2.right.y < signalData.vector.y ? -angle : angle);
            else
                transform.eulerAngles = new Vector3(0, 0, Vector2.right.y < signalData.vector.y ? angle : -angle);
        }

        public void Signal(AttackSignal signalData)
        {
            animator.SetTrigger("Attack");
            attack = signalData.attack;
        }

        public void Signal(SetReadySignal signalData)
        {
            if (!signalData.ready)
                Fsm.SetState<WeaponIdleState>();
        }

        public override void Update()
        {
            if (Fsm.behaviour.CurrentAmmo <= 0)
                Fsm.SetState<WeaponReloadState>();
            else
            {
                if (time <= 0 && attack)
                {
                    time = Fsm.behaviour.Speed;
                    var accuracy = Fsm.behaviour.Accuracy;
                    var Offset = new Vector3(Random.Range(-accuracy, accuracy), Random.Range(-accuracy, accuracy), 0);
                    Fsm.behaviour.CreateAttack(Offset);
                }
                else if (time > 0)
                {
                    time -= Time.deltaTime;
                }
            }
        }
    }
}