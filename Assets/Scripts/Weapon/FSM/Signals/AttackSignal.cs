namespace FSM.Scripts.Weapons.Signals
{
    public class AttackSignal : FsmSignal
    {
        public readonly bool attack;
        public AttackSignal(bool attack) { this.attack = attack; }
    }
}