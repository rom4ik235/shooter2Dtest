namespace FSM.Scripts.Weapons.Signals
{
    public class SetReadySignal : FsmSignal
    {
        public readonly bool ready;

        public SetReadySignal(bool ready) { this.ready = ready; }
    }
}