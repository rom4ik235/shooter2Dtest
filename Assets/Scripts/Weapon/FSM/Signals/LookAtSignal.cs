namespace FSM.Scripts.Weapons.Signals
{
    public class LookAtSignal : FsmSignal
    {
        public readonly UnityEngine.Vector2 vector;
        public readonly bool flip;

        public LookAtSignal(UnityEngine.Vector2 vector, bool flip)
        {
            this.vector = vector;
            this.flip = flip;
        }
    }
}