using FSM.Scripts.Weapons;
using FSM.Scripts.Weapons.Signals;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [field: SerializeField] public Transform LimbL { get; protected set; }
    [field: SerializeField] public Transform LimbR { get; protected set; }

    [SerializeField] protected LayerMask _layerMask;
    [SerializeField] protected Transform _firePoint;
    [SerializeField] protected BulletTrail _bullet;
    [SerializeField] protected AnimationClip _reloadAnimationClip;

    [field: SerializeField] public float Range { get; protected set; }
    [field: SerializeField] public int Damage { get; protected set; } = 1;
    [field: SerializeField, Range(0.1f, 0.5f)] public float Speed { get; protected set; }
    [field: SerializeField, Range(0f, 0.2f)] public float Accuracy { get; protected set; }
    [field: SerializeField, Range(1, 100)] public int Ammo { get; protected set; }
    public int CurrentAmmo { get; protected set; }

    protected Animator animator;
    protected Fsm<Weapon> Fsm;

    private void Awake()
    {
        CurrentAmmo = Ammo;
        animator = GetComponent<Animator>();
        Fsm = new Fsm<Weapon>(this);
        Fsm.AddState(new WeaponIdleState(Fsm, animator));
        Fsm.AddState(new WeaponReloadState(Fsm, animator, _reloadAnimationClip));
        Fsm.AddState(new WeaponReadyState(Fsm, animator));
        Fsm.SetState<WeaponIdleState>();
    }

    public void SetCurrentAmmo(int ammo) => CurrentAmmo = ammo;

    public void Attack(bool attack) => Fsm.Signal(new AttackSignal(attack));

    public void LookAt(Vector2 vector, bool flip) => Fsm.Signal(new LookAtSignal(vector, flip));

    public void SetReady(bool value) => Fsm.Signal(new SetReadySignal(value));

    public void CreateAttack(Vector3 Offset)
    {
        var right = transform.right + Offset;
        var hit = Physics2D.Raycast(_firePoint.position, right, Range, _layerMask);

        var trail = Instantiate(_bullet, _firePoint.position, transform.rotation);
        if (hit.collider != null)
        {
            trail.SetTargetPosition(hit.point);
            if (hit.transform.TryGetComponent<Enemy>(out var enemy))
                enemy.Damage(Damage);
        }
        else
            trail.SetTargetPosition(_firePoint.position + right * Range);
        CurrentAmmo--;
    }


    protected virtual void Update() => Fsm.CurrentState.Update();
}
