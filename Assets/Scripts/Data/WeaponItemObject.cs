using UnityEngine;


namespace Data
{
    [CreateAssetMenu(fileName = "WeaponItemObject", menuName = "Items/WeaponItemObject", order = 0)]
    public class WeaponItemObject : ItemObject
    {
        public override ItemTypeEnum Type() => ItemTypeEnum.Weapon;
        [field: SerializeField] public Weapon Prefab { get; private set; }
        [field: SerializeField] public Sprite Image { get; private set; }

        public override bool Equals(ItemObject other)
        {
            if (other is WeaponItemObject weaponItem)
            {
                return base.Equals(other) &&
                Prefab.Equals(weaponItem.Prefab) &&
                Image.Equals(weaponItem.Image);
            }
            return false;
        }
    }
}


