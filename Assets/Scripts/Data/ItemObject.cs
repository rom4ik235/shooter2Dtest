using Unity.Collections;
using UnityEngine;


namespace Data
{
    public enum ItemTypeEnum { Default, Weapon, Cloth, Ammo }
    abstract public class ItemObject : ScriptableObject, System.IEquatable<ItemObject>
    {
        public abstract ItemTypeEnum Type();


        [field: SerializeField] public Sprite Icon { get; protected set; }
        [field: SerializeField] public string Name { get; protected set; }


        public virtual bool Equals(ItemObject other)
        {
            return Type() == other.Type() &&
            Icon.Equals(other.Icon) &&
            Name.Equals(other.Name);
        }
    }
}


