using UnityEngine;


namespace Data
{
    [CreateAssetMenu(fileName = "AmmoItemObject", menuName = "Items/AmmoItemObject", order = 0)]
    public class AmmoItemObject : ItemObject
    {
        [field: SerializeField] public int Count { get; private set; }
        public override ItemTypeEnum Type() => ItemTypeEnum.Ammo;

        public override bool Equals(ItemObject other) => base.Equals(other);
    }
}


