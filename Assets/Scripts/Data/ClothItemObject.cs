using UnityEngine;


namespace Data
{
    [CreateAssetMenu(fileName = "ClothItemObject", menuName = "Items/ClothItemObject", order = 0)]
    public class ClothItemObject : ItemObject
    {
        public override ItemTypeEnum Type() => ItemTypeEnum.Cloth;
        // [field: SerializeField] public Cloth Prefab { get; private set; }
        [field: SerializeField] public Sprite Image { get; private set; }

        public override bool Equals(ItemObject other)
        {
            if (other is ClothItemObject weaponItem)
            {
                return base.Equals(other) &&
                Image.Equals(weaponItem.Image);
            }
            return false;
        }
    }
}


