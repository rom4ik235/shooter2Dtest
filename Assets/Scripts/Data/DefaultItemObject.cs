using UnityEngine;


namespace Data
{
    [CreateAssetMenu(fileName = "DefaultItemObject", menuName = "Items/DefaultItemObject", order = 0)]
    public class DefaultItemObject : ItemObject
    {
        [field: SerializeField] public int Count { get; private set; }


        public override ItemTypeEnum Type() => ItemTypeEnum.Default;


        public override bool Equals(ItemObject other)
        {
            if (other is DefaultItemObject weaponItem)
            {
                return base.Equals(other) &&
                Count.Equals(weaponItem.Count);
            }
            return false;
        }
    }
}


